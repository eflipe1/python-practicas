# link:https://www.pythontutorial.net/python-oop/python-inheritance/

class Person:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def introduce(self):
        return f"Hi. I'm {self.full_name}. I'm {self.age} years old."

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, value):
        if value <= 0:
            raise ValueError('Age is not valid')

        self.__age = value

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"


class Employee(Person):
    def __init__(self, first_name, last_name, age, job_title, salary):
        super().__init__(first_name, last_name, age)

        self.job_title = job_title
        self.salary = salary

    @property
    def job_title(self):
        return self.__job_title

    @job_title.setter
    def job_title(self, value):
        self.__job_title = value

    @property
    def salary(self):
        return self.__salary

    @salary.setter
    def salary(self, value):
        if value <= 0:
            raise ValueError('Salary must be greater than zero.')

        self.__salary = value

    # override method
    def introduce(self):
        introduction = super().introduce()
        introduction += f"I'm a {self.job_title}"
        return return


employee = Employee('John', 'Doe', 25)
employee.job_title = "Python Developer"
employee.salary = 120000
print(employee.introduce())

# Adding a constructor to the child class
