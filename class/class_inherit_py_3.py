# link: https://www.digitalocean.com/community/tutorials/understanding-class-inheritance-in-python-3
# 1 2 3

class Fish:
    '''Parente class'''

    def __init__(self, first_name, last_name="Fish",
                 skeleton="bone", eyelids=False):
        self.first_name = first_name
        self.last_name = last_name
        self.skeleton = skeleton
        self.eyelids = eyelids

    def swim(self):
        print("The fish is swimming.")

    def swim_backwards(self):
        print("The fish can swim backwards.")

# se puede construir una clase sin atributos ni pedos


class FishHijo(Fish):
    '''With child classes, we can choose to add more methods,
    override existing parent methods'''
    pass


class OtroFish(Fish):
    '''another child class that includes its own method'''

    def live_with_anemone(self):
        print("The clownfish is coexisting with sea anemone.")


# Overrinding
...


class Shark(Fish):
    '''we’ll be overriding the __init__() constructor method and the swim_backwards()'''

    def __init__(self, first_name, last_name="Shark",
                 skeleton="cartilage", eyelids=True):
        self.first_name = first_name
        self.last_name = last_name
        self.skeleton = skeleton
        self.eyelids = eyelids

    def swim_backwards(self):
        print("The shark cannot swim backwards, but can sink backwards.")

# super() Function


class Trout(Fish):
    def __init__(self, water="freshwater"):
        self.water = water
        super().__init__(self)

    def show_parent(self):
        print("SELF", )


# terry = FishHijo("Terry")
# print(terry.first_name + " " + terry.last_name)
# print(terry.skeleton)
# print(terry.eyelids)
# terry.swim()
# terry.swim_backwards()
# casey = OtroFish("Casey")
# print(casey.first_name + " " + casey.last_name)
# casey.swim()
# casey.live_with_anemone()
# sammy = Shark("Sammy")
# print(sammy.first_name + " " + sammy.last_name)
# sammy.swim()
# sammy.swim_backwards()
# print(sammy.eyelids)
# print(sammy.skeleton)
terry = Trout()

# Initialize first name
terry.first_name = "Terry"

# Use parent __init__() through super()
print(terry.first_name + " " + terry.last_name)
print(terry.eyelids)

# Use child __init__() override
print(terry.water)

# Use parent swim() method
terry.swim()
