# link: https://blog.usejournal.com/playing-with-inheritance-in-python-73ea4f3b669es

class Person:  # base class or parent class
    def __init__(self, name):  # constructor
        self.name = name

    def get_name(self):
        return self.name


class Employee(Person):  # derived class or subclass
    def is_employee(self):
        return True


# person = Person("Pythonista")  # object creation/instantiation
# print(person.get_name())
#
# employee = Employee("Employee Pythonista")
# print(employee.get_name())
# print(employee.is_employee())

# https://blog.usejournal.com/playing-with-inheritance-in-python-73ea4f3b669e
class A:
    def speak(self):
        print("class A speaking")


class B:
    def speak(self):
        print("class B speaking")


class C(A, B):
    def speak(self, num_1, num_2=2):  # override
        print(num_1 + num_2)
        print("class C speaking")


c = C()
c.speak(5)

# super()


class A:
    def test(self):
        return 'A'


class B(A):
    def test(self):  # override test method
        return "B" + super().test()  # access method of parent class to overridden child class


class B(A):
    def test(self):
        return "B" + super(B, self).test()  # super(className,object)


print(B().test())


'''
Difference between super() and super(className, self):

Python 3 encourages using super(),
instead of using super(className,self) ,
both have the same effect. Python 2, only
supports the super(className,self) syntax.
Since, Python 2 is widely used so Python 3 also
has support for this type of super calling
'''


'''
isinstance() : isinstance() takes two argument :
an object and a class. It returns True if the
given class is anywhere in the inheritance
chain of the object’s class.
'''

print(isinstance(A(), A))
print(isinstance(B(), A))


'''
issubclass() : issubclass() takes two argument
(class, class).It returns True if the first class
contains the second class anywhere in its inheritance
chain
'''


class A:
    def test(self):
        return 'A'


class B(A):
    def test(self):
        return "B" + super(B, self).test()


class C(A):
    pass


print(issubclass(C, A))
print(issubclass(A, A))

'''
__bases__() : __bases__() provides a tuple of
immediate base classes of a class
'''


class A:
    def test(self):
        return 'A'


class B(A):
    def test(self):
        return "B" + super(B, self).test()


class C:
    pass


class D(A, C):
    pass


print(A.__bases__)
print(B.__bases__)
print(C.__bases__)
print(D.__bases__)

'''
__subclasses__() : __subclasses__() returns
a list of all
the subclasses a class. Like __bases__() ,
__subclasses__ only goes one level deep from
the class we’re working on
'''
print("__subclasses__()")
print(A.__subclasses__())
print(object.__subclasses__())

print("__mro__ : __mro__ is an attributes which contains full MRO (Method Resolution Order) of a class as a tuple")
print(D.__mro__)
